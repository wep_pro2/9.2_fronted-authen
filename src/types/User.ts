import type { Role } from "./Role"
type Gender = 'male' | 'female' | 'others'
type User = {
  id?: number
  email: string
  image: string
  password: string
  fullName: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
}

export type { Gender, User }
